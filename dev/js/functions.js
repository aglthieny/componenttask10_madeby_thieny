$(document).ready(function(){

	var $status = $('.c-numberslide');
	var $slickElement = $('.c-slide1__block');

	$slickElement.on('init reInit beforeChange', function (event, slick, currentSlide, nextSlide) {
	  var i = (nextSlide ? nextSlide : 0) + 1;
	  var count = (slick.slideCount > 9) ? (slick.slideCount) : ('0' + slick.slideCount);
	  if(i < 10){
	  	$status.text('0'+ i + ' / ' + count);
	  }else{
	  	$status.text(i + ' / ' + count);
	  }
	});

	//slide1
	$slickElement.slick({
	  speed: 1000,
	  variableWidth: true,
	  arrows: true,
	  //autoplay: true,
	  autoplaySpeed: 3000,
	  prevArrow:"<span class='prev'><img src='/assets/img/common/icon1-slide.png'></span>",
	  nextArrow:"<span class='next'><img src='/assets/img/common/icon1-slide.png'></span>",
	});

	
	var wbody = $('body').width();
	var wwrap1 = $('.l-wrap1').width();
	var wwrap2 = $('.l-wrap3').width();
	var wslide = (wbody - wwrap1 - 20)/2;
	var lnext = wslide + 40;
	var rprev = wslide + 100;
	var rcount = wslide + 140;

	$('.c-slide1').css('margin-left', wslide);
	$('.next').css('right', lnext);
	$('.prev').css('right', rprev);
	$('.c-numberslide').css('right', rcount);

	$(window).resize(function(){
		var wbody = $('body').width();
		var wwrap1 = $('.l-wrap1').width();
		var wwrap2 = $('.l-wrap3').width();
		var wslide = (wbody - wwrap1 - 20)/2;
		var lnext = wslide + 40;
		var rprev = wslide + 100;
		var rcount = wslide + 140;

		$('.next').css('right', lnext);
		$('.prev').css('right', rprev);
		$('.c-numberslide').css('right', rcount);
		$('.c-slide1').css('margin-left', wslide);
	});


	$('.c-navi1__tab').click(function(){
		var tab = $(this).data('tab');
		$('.c-navi1__tab').removeClass('is-active');
		$(this).addClass('is-active');

		$('.c-list6__item').hide();
		$('.'+tab).show();
		if(tab=="all"){
			$('.c-list6__item').show();
		}
	});

});

//Menu SP
$(document).ready(function() {
  //var body = $("body");
  var submenu = $(".c-submenu"),
  ovlay = $(".overlay"),
  targetli = $(".c-gnavi > ul > li"),
  length, width;
  submenu.hide();
  	
    targetli.mouseover(function() {
    	length = $(this).find(submenu).children().length;
    	width = $(window).width();

    	if ( length !== 0 && width >= 768) {
        	$(this).find(submenu).stop(true,true).delay(300).slideDown(350);
        	//$(this).children('a').addClass('active');
    	}
    }).mouseout(function(){
    	if ( length !== 0 && width >= 768) {
        	$(this).find(submenu).stop(true,true).delay(100).slideUp(350);
        	//$(this).children('a').removeClass('active');
        }
    });
});


//js btn contact
var clickLink = $('.c-hcontact__link'),
	contactH = $('.c-hcontact__info');
clickLink.on('click', function () {
	$(this).toggleClass('active');
	contactH.stop().slideToggle('fast');
	return false;
});