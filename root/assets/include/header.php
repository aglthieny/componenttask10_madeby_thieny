<!DOCTYPE html>
<html lang="ja" id="pagetop">
<head>
<meta charset="UTF-8">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<?php include('meta.php'); ?>
<link href="/assets/css/style.min.css" rel="stylesheet">
<script src="/assets/js/jquery-3.3.1.min.js"></script>
<script src="/assets/js/jquery-migrate-3.0.1.min.js"></script>
</head>
<body class="page-<?php echo $pageid; ?>">
<header class="c-header">
	<div class="c-header-top">
		<div class="c-header-top__left">
			<a class="c-header-top__txt1" href="" title="">NSDビジネスイノベーション TOP</a>
		</div>
		<div class="c-header-top__right">
			<a class="c-header-top__txt1" href="" title="">ユーザー様専用サポート</a>
			<p class="c-header-top__txt2">電話でのご相談</p>
			<ul class="c-header-top__hotline">
				<li>
					<b>東京</b>
					<a href="TEL:0332571141" title="">03-3257-1141</a>
				</li>
				<li>
					<b>大阪</b>
					<a href="TEL:0664421329" title="">06-6442-1329</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="c-header__inner">
		<div class="c-logo">
			<h1>
				<a class="c-logo__txt" href="/" title=""><img src="/assets/img/common/logo.png" title=""></a>
			</h1>
		</div>
		<nav class="c-gnavi">
			<ul>
				<li><a href="" title="">特長</a></li>
				<li class="nav-sub">
					<a href="" title="">機能・仕様</a>
					<div class="c-submenu">
						<div class="c-submenu__block">
							<h3 class="c-submenu__ttl1">機能・仕様</h3>
							<ul>
								<li><a href="" title="">機能</a></li>
								<li><a href="" title="">カスタマイズ例</a></li>
								<li><a href="" title="">セキュリティ対策</a></li>
							</ul>
							<ul>
								<li><a href="" title="">オプション機能</a></li>
								<li><a href="" title="">動作環境</a></li>
							</ul>
							<ul>
								<li><a href="" title="">連携機能</a></li>
								<li><a href="" title="">サーバー・システム構成例</a></li>
							</ul>
						</div>
					</div>
				</li>
				<li><a href="" title="">料金プラン</a></li>
				<li class="nav-sub"><a href="" title="">活用シーン</a></li>
				<li><a href="" title="">導入ガイド</a></li>
				<li><a href="" title="">よくあるご質問</a></li>
				<li class="nav-sub"><a href="" title="">お問い合わせ</a></li>
			</ul>
		</nav>
		<div class="c-hcontact">
			<a class="c-hcontact__link" href="" title=""><span>お問い合わせ</span></a>
			<div class="c-hcontact__info">
				<div class="c-list2 c-list2--with">
					<ul>
						<li class="c-list2__item c-list2__first">
							<div class="c-ttl4 c-ttl4--white">
								<p class="c-ttl4__icon"><img src="/assets/img/common/icon7-ttl4.png" alt=""></p>
								<h3 class="c-ttl4__txt">電話でのご相談</h3>
							</div>
							<p class="c-list2__hotline"><span>東京</span><a href="TEL:0332571141" title=""><img src="/assets/img/common/hotline1.png" alt=""></a></p>
							<p class="c-list2__hotline"><span>大阪</span><a href="TEL:0664421329" title=""><img src="/assets/img/common/hotline2.png" alt=""></a></p>
							<p class="c-list2__open"><b>受付時間</b><span>月～金9:00～12:00／13:00～17:00<br>（土日祝日、年末年始を除く）</span></p>
						</li>
						<li class="c-list2__item">
							<div class="c-ttl4 c-ttl4--white">
								<p class="c-ttl4__icon"><img src="/assets/img/common/icon8-ttl4.png" alt=""></p>
								<h3 class="c-ttl4__txt">メールでのご相談</h3>
							</div>
							<p class="c-list2__txt">ご質問やご要望など、お気軽にご相談ください。担当者から3営業日以内にご連絡いたします。</p>
							<div class="c-btn1 c-btn1--blue">
								<a class="c-btn1__txt" href="" title=""><span>お問い合わせフォーム</span></a>
							</div>
						</li>
						<li class="c-list2__item">
							<div class="c-ttl4 c-ttl4--white">
								<p class="c-ttl4__icon"><img src="/assets/img/common/icon9-ttl4.png" alt=""></p>
								<h3 class="c-ttl4__txt">資料のご請求</h3>
							</div>
							<p class="c-list2__txt">NSDビジネスイノベーションがご提供する製品・サービスの各種カタログをお申し込みいただけます。</p>
							<div class="c-btn1 c-btn1--blue">
								<a class="c-btn1__txt" href="" title=""><span>資料請求フォーム</span></a>
							</div>
						</li>
					</ul>
				</div>
				</div>
		</div>
	</div>
</header>