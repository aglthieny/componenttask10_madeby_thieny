<?php /*========================================
slide
================================================*/ ?>
<div class="c-dev-title1">slide</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-slide1</div>
<div class="c-slide1">
	<div class="c-slide1__block">
		<div class="c-slide1__item">
			<div class="c-slide1__img">
				<img src="/assets/img/common/img1-slide1.jpg" alt="">
			</div>
			<div class="c-slide1__info">
				<div class="c-slide1__ttl">
					<img src="/assets/img/common/img1-slide1.jpg" alt="">
					<h3 class="c-slide1__txt1"><span>テレワークに必須！大容量ファイルを簡単・安全に送信するメソッド</span></span></h3>
				</div>
				<p class="c-slide1__txt2">リード文が入ります。この文章はダミーです予めご了承ください。リード文が入ります。この文章はダミーです予めご了承くださ</p>
				<div class="c-time1"><img src="/assets/img/common/icon-clock.png" alt=""><span>2019.03.01 8時間前</span></div>
				<div class="c-tags">
					<a href="" title="">#タグ</a>,<a href="" title=""> #タグ</a>
				</div>
			</div>
			<div class="c-cats  c-cats--blue1">
				<p class="c-cats__txt">セキュリティ</p>
			</div>
			<a class="c-slide1__link" href="" title=""></a>
		</div>
		<div class="c-slide1__item">
			<div class="c-slide1__img">
				<img src="/assets/img/common/img2-slide1.jpg" alt="">
			</div>
			<div class="c-slide1__info">
				<div class="c-slide1__ttl">
					<img src="/assets/img/common/img2-slide1.jpg" alt="">
					<h3 class="c-slide1__txt1"><span>
					テレワークに必須！大容量ファイルを簡単・安全に送信するメソッド</span></h3>
				</div>
				<p class="c-slide1__txt2">リード文が入ります。この文章はダミーです予めご了承ください。リード文が入ります。この文章はダミーです予めご了承くださ</p>
				<div class="c-time1"><img src="/assets/img/common/icon-clock.png" alt=""><span>2019.03.01 8時間前</span></div>
				<div class="c-tags">
					<a href="" title="">#タグ</a>,<a href="" title=""> #タグ</a>
				</div>
			</div>
			<div class="c-cats c-cats--green1">
				<p class="c-cats__txt ">業務効率化・コスト削減</p>
			</div>
			<a class="c-slide1__link" href="" title=""></a>
		</div>
		<div class="c-slide1__item">
			<div class="c-slide1__img">
				<img src="/assets/img/common/img1-slide1.jpg" alt="">
			</div>
			<div class="c-slide1__info">
				<div class="c-slide1__ttl">
					<img src="/assets/img/common/img1-slide1.jpg" alt="">
					<h3 class="c-slide1__txt1"><span>
					テレワークに必須！大容量ファイルを簡単・安全に送信するメソッド</span></h3>
				</div>
				<p class="c-slide1__txt2">リード文が入ります。この文章はダミーです予めご了承ください。リード文が入ります。この文章はダミーです予めご了承くださ</p>
				<div class="c-time1"><img src="/assets/img/common/icon-clock.png" alt=""><span>2019.03.01 8時間前</span></div>
				<div class="c-tags">
					<a href="" title="">#タグ</a>,<a href="" title=""> #タグ</a>
				</div>
			</div>
			<div class="c-cats c-cats--green2">
				<p class="c-cats__txt">スマートデバイス活用</p>
			</div>
			<a class="c-slide1__link" href="" title=""></a>
		</div>
		<div class="c-slide1__item">
			<div class="c-slide1__img">
				<img src="/assets/img/common/img1-slide1.jpg" alt="">
			</div>
			<div class="c-slide1__info">
				<div class="c-slide1__ttl">
					<img src="/assets/img/common/img1-slide1.jpg" alt="">
					<h3 class="c-slide1__txt1"><span>
					テレワークに必須！大容量ファイルを簡単・安全に送信するメソッド</span></h3>
				</div>
				<p class="c-slide1__txt2">リード文が入ります。この文章はダミーです予めご了承ください。リード文が入ります。この文章はダミーです予めご了承くださ</p>
				<div class="c-time1"><img src="/assets/img/common/icon-clock.png" alt=""><span>2019.03.01 8時間前</span></div>
				<div class="c-tags">
					<a href="" title="">#タグ</a>,<a href="" title=""> #タグ</a>
				</div>
			</div>
			<div class="c-cats  c-cats--blue1">
				<p class="c-cats__txt">製品・サービス</p>
			</div>
			<a class="c-slide1__link" href="" title=""></a>
		</div>
		<div class="c-slide1__item">
			<div class="c-slide1__img">
				<img src="/assets/img/common/img1-slide1.jpg" alt="">
			</div>
			<div class="c-slide1__info">
				<div class="c-slide1__ttl">
					<img src="/assets/img/common/img1-slide1.jpg" alt="">
					<h3 class="c-slide1__txt1"><span>
					テレワークに必須！大容量ファイルを簡単・安全に送信するメソッド</span></h3>
				</div>
				<p class="c-slide1__txt2">リード文が入ります。この文章はダミーです予めご了承ください。リード文が入ります。この文章はダミーです予めご了承くださ</p>
				<div class="c-time1"><img src="/assets/img/common/icon-clock.png" alt=""><span>2019.03.01 8時間前</span></div>
				<div class="c-tags">
					<a href="" title="">#タグ</a>,<a href="" title=""> #タグ</a>
				</div>
			</div>
			<div class="c-cats  c-cats--blue1">
				<p class="c-cats__txt">製品・サービス</p>
			</div>
			<a class="c-slide1__link" href="" title=""></a>
		</div>
	</div>
	<div class="c-numberslide"></div>
	<div class="c-btn1 c-btn1--blue">
		<a class="c-btn1__txt" href="" title=""><span>導入ガイド一覧</span></a>
	</div>
</div>


<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-slide1 c-slide1--width</div>
<div class="c-slide1 c-slide1--width">
	<div class="c-slide1__block">
		<div class="c-slide1__item">
			<div class="c-slide1__img">
				<img src="/assets/img/common/img1-slide1.jpg" alt="">
			</div>
			<div class="c-slide1__info">
				<div class="c-slide1__ttl">
					<img src="/assets/img/common/img1-slide1.jpg" alt="">
					<h3 class="c-slide1__txt1"><span>テレワークに必須！大容量ファイルを簡単・安全に送信するメソッド</span></h3>
				</div>
				<p class="c-slide1__txt2">リード文が入ります。この文章はダミーです予めご了承ください。リード文が入ります。この文章はダミーです予めご了承くださ</p>
				<div class="c-time1"><span>2019.03.01</span></div>
			</div>
			<div class="c-cats  c-cats--blue1">
				<p class="c-cats__txt">セキュリティ</p>
			</div>
			<a class="c-slide1__link" href="" title=""></a>
		</div>
		<div class="c-slide1__item">
			<div class="c-slide1__img">
				<img src="/assets/img/common/img2-slide1.jpg" alt="">
			</div>
			<div class="c-slide1__info">
				<div class="c-slide1__ttl">
					<img src="/assets/img/common/img2-slide1.jpg" alt="">
					<h3 class="c-slide1__txt1"><span>
					テレワークに必須！大容量ファイルを簡単・安全に送信するメソッド</span></h3>
				</div>
				<p class="c-slide1__txt2">リード文が入ります。この文章はダミーです予めご了承ください。リード文が入ります。この文章はダミーです予めご了承くださ</p>
				<div class="c-time1"><span>2019.03.01</span></div>
			</div>
			<div class="c-cats c-cats--green1">
				<p class="c-cats__txt ">業務効率化・コスト削減</p>
			</div>
			<a class="c-slide1__link" href="" title=""></a>
		</div>
		<div class="c-slide1__item">
			<div class="c-slide1__img">
				<img src="/assets/img/common/img1-slide1.jpg" alt="">
			</div>
			<div class="c-slide1__info">
				<div class="c-slide1__ttl">
					<img src="/assets/img/common/img1-slide1.jpg" alt="">
					<h3 class="c-slide1__txt1"><span>
					テレワークに必須！大容量ファイルを簡単・安全に送信するメソッド</span></h3>
				</div>
				<p class="c-slide1__txt2">リード文が入ります。この文章はダミーです予めご了承ください。リード文が入ります。この文章はダミーです予めご了承くださ</p>
				<div class="c-time1"><span>2019.03.01</span></div>
			</div>
			<div class="c-cats c-cats--green2">
				<p class="c-cats__txt">スマートデバイス活用</p>
			</div>
			<a class="c-slide1__link" href="" title=""></a>
		</div>
	</div>
	<div class="c-numberslide"></div>
	<div class="c-btn1 c-btn1--blue">
		<a class="c-btn1__txt" href="" title=""><span>導入ガイド一覧</span></a>
	</div>
</div>
