<?php /*========================================
btn
================================================*/ ?>
<div class="c-dev-title1">btn</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-btn1</div>
<div class="c-btn1">
	<a class="c-btn1__txt" href="" title=""><span>お役立ち資料ダウンロード</span></a>
</div>


<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-btn1 c-btn1--blue</div>
<div class="c-btn1 c-btn1--blue">
	<a class="c-btn1__txt" href="" title=""><span>便利な機能を見る</span></a>
</div>


<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-btn2</div>
<div class="c-btn2">
	<a class="c-btn2__txt" href="" title=""><span>詳しく見る</span></a>
</div>
