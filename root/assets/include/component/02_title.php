<?php /*========================================
title
================================================*/ ?>
<div class="c-dev-title1">title</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-ttl1</div>
<div class="l-wrap1">
	<div class="c-ttl1">
		<div class="c-ttl1__left">
			<p class="c-ttl1__txt1"><img src="/assets/img/common/txt1-ttl1.png" alt=""></p>
			<h2 class="c-ttl1__txt2">eTransporterとは</h2>
		</div>
	</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-ttl1 c-ttl1--text</div>
<div class="l-wrap1">
	<div class="c-ttl1 c-ttl1--text">
		<div class="c-ttl1__left">
			<p class="c-ttl1__txt1"><img src="/assets/img/common/txt2-ttl1.png" alt=""></p>
			<h2 class="c-ttl1__txt2">導入ガイド</h2>
		</div>
		<div class="c-ttl1__right">
			<p>導入ガイドの概要を記載します。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。（100文字前後）</p>
		</div>
	</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-ttl2</div>
<div class="l-wrap2">
	<div class="c-ttl2">
		<div class="c-ttl2__left">
			<h2 class="c-ttl2__txt1"><img src="/assets/img/common/txt1-ttl2.png" alt=""></h2>
			<p class="c-ttl2__txt2">NBIのソリューションの特長</p>
		</div>
	</div>
</div>


<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-ttl2 c-ttl2--text</div>
<div class="l-wrap2">
	<div class="c-ttl2 c-ttl2--text">
		<div class="c-ttl2__left">
			<h2 class="c-ttl2__txt1"><img src="/assets/img/common/txt2-ttl2.png" alt=""></h2>
			<p class="c-ttl2__txt2">NBIがご提供する製品・サービス</p>
		</div>
		<div class="c-ttl2__right">
			<p>製品・サービス概要を記載します。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。（100文字前後）</p>
		</div>
	</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-ttl3</div>
<div class="c-ttl3">
	<div class="c-ttl3__rank">
		<span class="c-ttl3__txt2">理由</span>
		<p class="c-ttl3__txt3"><img src="/assets/img/common/txt1-ttl3.png" alt=""></p>
	</div>
	<h3 class="c-ttl3__txt1">理由01が入ります（20文字前後）</h3>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-ttl3 c-ttl3--width</div>
<div class="c-ttl3 c-ttl3--width">
	<p class="c-ttl3__txt4"><img src="/assets/img/common/txt2-ttl3.png" alt=""></p>
	<h3 class="c-ttl3__txt1">ソリューションサービス</h3>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-ttl4</div>
<div class="c-ttl4">
	<p class="c-ttl4__icon"><img src="/assets/img/common/icon1-ttl4.png" alt=""></p>
	<h3 class="c-ttl4__txt">利便性への<br class="pc-only">あくなき追求</h3>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-ttl4 c-ttl4--green</div>
<div class="c-ttl4 c-ttl4--green">
	<p class="c-ttl4__icon"><img src="/assets/img/common/icon1-ttl4.png" alt=""></p>
	<h3 class="c-ttl4__txt">電話でのご相談</h3>
</div>


<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-ttl4 c-ttl4--white</div>
<div class="c-ttl4 c-ttl4--white">
	<p class="c-ttl4__icon"><img src="/assets/img/common/icon7-ttl4.png" alt=""></p>
	<h3 class="c-ttl4__txt">電話でのご相談</h3>
</div>


<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-ttl5</div>
<div class="c-ttl5">
	<p class="c-ttl5__icon"><img src="/assets/img/common/txt1-ttl4.png" alt=""></p>
	<div class="c-ttl5__detail">
		<h3 class="c-ttl5__txt1">ファイル転送システムに運用コストをかけたくない！25文字前後</h3>
		<ul class="c-ttl5__info">
			<li>
				<span>業種</span>
				<p>サービス業</p>
			</li>
			<li>
				<span>部署</span>
				<p>システム部</p>
			</li>
			<li>
				<span>規模</span>
				<p>500名</p>
			</li>
		</ul>
	</div>
</div>