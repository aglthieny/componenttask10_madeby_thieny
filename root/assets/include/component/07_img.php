<?php /*========================================
img
================================================*/ ?>
<div class="c-dev-title1">img</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-imgtxt1</div>
<div class="l-wrap1">
<div class="c-imgtxt1">
	<div class="c-imgtxt1__info">
		<div class="c-ttl3 c-ttl3--width">
			<h3 class="c-ttl3__txt1">大企業・官公庁への導入実績多数！<br>
			大容量ファイルを「簡単・安全」に送受信できるファイル転送システム</h3>
		</div>
		<p>eTransporter（略称、eTra）は、メールに添付できないような大きなファイルや機密性の高いデータを企業間でセキュアに送信・共有できるファイル転送システムです。</p>
		<p>監査ログ、ファイル自動暗号化に標準で対応し、オプションでウイルスチェック、キーワードフィルター、上長承認にも対応し、セキュリティ面でも安心してファイルを送信することができます。この文章（200文字前後）</p>
	</div>
	<div class="c-imgtxt1__img">
		<img src="/assets/img/common/img1-imgtxt1.jpg" alt="">
	</div>
</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-imgtxt1 c-imgtxt1--custom</div>
<div class="l-wrap1">
<div class="c-imgtxt1 c-imgtxt1--custom">
	<div class="c-imgtxt1__info">
		<div class="c-ttl3">
			<div class="c-ttl3__rank">
				<span class="c-ttl3__txt2">理由</span>
				<p class="c-ttl3__txt3"><img src="/assets/img/common/txt1-ttl3.png" alt=""></p>
			</div>
			<h3 class="c-ttl3__txt1">理由01が入ります（20文字前後）</h3>
		</div>
		<p>選ばれる理由01の説明が入ります。この文章はダミーです予めご<br class="pc-only">了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。（200文字前後）</p>
		<div class="c-btn2">
			<a class="c-btn2__txt" href="" title=""><span>詳しく見る</span></a>
		</div>
	</div>
	<div class="c-imgtxt1__img">
		<img src="/assets/img/common/img2-imgtxt1.jpg" alt="">
	</div>
</div>
</div>


<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-imgtxt1 c-imgtxt1--custom c-imgtxt1--right</div>
<div class="l-wrap1">
<div class="c-imgtxt1 c-imgtxt1--custom c-imgtxt1--right">
	<div class="c-imgtxt1__info">
		<div class="c-ttl3">
			<div class="c-ttl3__rank">
				<span class="c-ttl3__txt2">理由</span>
				<p class="c-ttl3__txt3"><img src="/assets/img/common/txt4-ttl3.png" alt=""></p>
			</div>
			<h3 class="c-ttl3__txt1">理由02が入ります（20文字前後）</h3>
		</div>
		<p>選ばれる理由02の説明が入ります。この文章はダミーです予めご<br class="pc-only">了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。（200文字前後）</p>
		<div class="c-btn2">
			<a class="c-btn2__txt" href="" title=""><span>詳しく見る</span></a>
		</div>
	</div>
	<div class="c-imgtxt1__img">
		<img src="/assets/img/common/img2-imgtxt2.jpg" alt="">
	</div>
</div>
</div>


<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-imgtxt2</div>
<div class="l-wrap2">
<div class="c-imgtxt2">
	<div class="c-imgtxt2__info">
		<div class="c-ttl3 c-ttl3--width">
			<p class="c-ttl3__txt4"><img src="/assets/img/common/txt2-ttl3.png" alt=""></p>
			<h3 class="c-ttl3__txt1">ソリューションサービス</h3>
		</div>
		<p>お客様の業務を理解・分析し、的確な製品・サービスの組み合わせやカスタマイズ提案によって、課題解決いたします。さらに、お客様のニーズやご意見を収集し、ＮＳＤのＲ＆Ｄ部門と連携して新しいソリューションの創出に取り組みます。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。（200文字前後）</p>
	</div>
	<div class="c-imgtxt2__img">
		<img src="/assets/img/common/img1-imgtxt2.jpg" alt="">
	</div>
</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-imgtxt2 c-imgtxt2__right</div>
<div class="l-wrap2">
<div class="c-imgtxt2 c-imgtxt2__right">
	<div class="c-imgtxt2__info">
		<div class="c-ttl3 c-ttl3--width">
			<p class="c-ttl3__txt4"><img src="/assets/img/common/txt3-ttl3.png" alt=""></p>
			<h3 class="c-ttl3__txt1">システムインテグレーション</h3>
		</div>
		<p>3,000名を超える技術者集団であるＮＳＤグループの強みを活かし、既存の製品・サービスにとらわれない解決策を、ＮＳＤの各事業部と連携してご提案いたします。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。（200文字前後）</p>
	</div>
	<div class="c-imgtxt2__img">
		<img src="/assets/img/common/img2-imgtxt2.jpg" alt="">
	</div>
</div>
</div>


<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-imgtxt3</div>
<section class="c-feature">
	
	<div class="c-imgtxt3">
		<div class="c-imgtxt3__left">
			<div class="c-ttl2">
				<div class="c-ttl2__left">
					<h2 class="c-ttl2__txt1"><img src="/assets/img/common/txt1-ttl2.png" alt=""></h2>
					<p class="c-ttl2__txt2">NBIのソリューションの特長</p>
				</div>
			</div>
			<div class="c-imgtxt3__img" style="background: url(assets/img/common/img-feture.jpg) no-repeat right center/cover;"></div>
		</div>
		<div class="c-imgtxt3__right">
			<div class="c-imgtxt3__pr">
				<div class="c-imgtxt3__box">
					<h3 class="c-imgtxt3__ttl">業務改善ソリューションをご提供し、企業ビジネスの価値を高める</h3>
					<p class="c-imgtxt3__txt">NBIの強みを記載します。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。この文章はダミーです予めご了承ください。（200文字前後）</p>
				</div>
			</div>
		</div>
	</div>
</section>