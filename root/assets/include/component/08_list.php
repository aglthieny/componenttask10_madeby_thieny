<?php /*========================================
list
================================================*/ ?>
<div class="c-dev-title1">list</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list1</div>
<div class="l-wrap1">
<div class="c-list1">
	<ul>
		<li class="c-list1__item">
			<div class="c-ttl4">
				<p class="c-ttl4__icon"><img src="/assets/img/common/icon1-ttl4.png" alt=""></p>
				<h3 class="c-ttl4__txt">利便性への<br class="pc-only">あくなき追求</h3>
			</div>
			<p class="c-list1__txt">操作性や機能と言った利便性を追求し、開発を行っているため、誰も使い易く、長くご利用頂ける製品となっています。</p>
			<div class="c-btn1 c-btn1--blue">
				<a class="c-btn1__txt" href="" title=""><span>便利な機能を見る</span></a>
			</div>
		</li>
		<li class="c-list1__item">
			<div class="c-ttl4">
				<p class="c-ttl4__icon"><img src="/assets/img/common/icon2-ttl4.png" alt=""></p>
				<h3 class="c-ttl4__txt">セキュリティに<br class="pc-only">とことんこだわる</h3>
			</div>
			<p class="c-list1__txt">企業のセキュリティポリシーに合わせ、キメ細かい設定を行うことができ、お客様に安心をお届けいたします。</p>
			<div class="c-btn1 c-btn1--blue">
				<a class="c-btn1__txt" href="" title=""><span>セキュリティ機能を見る</span></a>
			</div>
		</li>
		<li class="c-list1__item">
			<div class="c-ttl4">
				<p class="c-ttl4__icon"><img src="/assets/img/common/icon3-ttl4.png" alt=""></p>
				<h3 class="c-ttl4__txt">管理運用が<br class="pc-only">ラクになる</h3>
			</div>
			<p class="c-list1__txt">管理運用がラクになる説明が入ります。この文章はダミーです予めご了承下さい。この文章はダミーです予めご了承下さい。</p>
			<div class="c-btn1 c-btn1--blue">
				<a class="c-btn1__txt" href="" title=""><span>管理運用方法を見る</span></a>
			</div>
		</li>
	</ul>
</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list2</div>
<div class="l-wrap1">
<div class="c-list2">
	<ul>
		<li class="c-list2__item c-list2__first">
			<div class="c-ttl4 c-ttl4--green">
				<p class="c-ttl4__icon"><img src="/assets/img/common/icon4-ttl4.png" alt=""></p>
				<h3 class="c-ttl4__txt">電話でのご相談</h3>
			</div>
			<p class="c-list2__hotline"><span>東京</span><a href="TEL:0332571141" title=""><img src="/assets/img/common/hotline1.png" alt=""></a></p>
			<p class="c-list2__hotline"><span>大阪</span><a href="TEL:0664421329" title=""><img src="/assets/img/common/hotline2.png" alt=""></a></p>
			<p class="c-list2__open"><b>受付時間</b><span>月～金9:00～12:00／13:00～17:00<br>（土日祝日、年末年始を除く）</span></p>
		</li>
		<li class="c-list2__item">
			<div class="c-ttl4 c-ttl4--green">
				<p class="c-ttl4__icon"><img src="/assets/img/common/icon5-ttl4.png" alt=""></p>
				<h3 class="c-ttl4__txt">メールでのご相談</h3>
			</div>
			<p class="c-list2__txt">ご質問やご要望など、お気軽にご相談ください。担当者から3営業日以内にご連絡いたします。</p>
			<div class="c-btn1 c-btn1--blue">
				<a class="c-btn1__txt" href="" title=""><span>お問い合わせフォーム</span></a>
			</div>
		</li>
		<li class="c-list2__item">
			<div class="c-ttl4 c-ttl4--green">
				<p class="c-ttl4__icon"><img src="/assets/img/common/icon6-ttl4.png" alt=""></p>
				<h3 class="c-ttl4__txt">資料のご請求</h3>
			</div>
			<p class="c-list2__txt">NSDビジネスイノベーションがご提供する製品・サービスの各種カタログをお申し込みいただけます。</p>
			<div class="c-btn1 c-btn1--blue">
				<a class="c-btn1__txt" href="" title=""><span>資料請求フォーム</span></a>
			</div>
		</li>
	</ul>
</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list2 c-list2--with</div>
<div class="l-wrap3">
<div class="c-list2 c-list2--with">
	<ul>
		<li class="c-list2__item c-list2__first">
			<div class="c-ttl4 c-ttl4--white">
				<p class="c-ttl4__icon"><img src="/assets/img/common/icon7-ttl4.png" alt=""></p>
				<h3 class="c-ttl4__txt">電話でのご相談</h3>
			</div>
			<p class="c-list2__hotline"><span>東京</span><a href="TEL:0332571141" title=""><img src="/assets/img/common/hotline1.png" alt=""></a></p>
			<p class="c-list2__hotline"><span>大阪</span><a href="TEL:0664421329" title=""><img src="/assets/img/common/hotline2.png" alt=""></a></p>
			<p class="c-list2__open"><b>受付時間</b><span>月～金9:00～12:00／13:00～17:00<br>（土日祝日、年末年始を除く）</span></p>
		</li>
		<li class="c-list2__item">
			<div class="c-ttl4 c-ttl4--white">
				<p class="c-ttl4__icon"><img src="/assets/img/common/icon8-ttl4.png" alt=""></p>
				<h3 class="c-ttl4__txt">メールでのご相談</h3>
			</div>
			<p class="c-list2__txt">ご質問やご要望など、お気軽にご相談ください。担当者から3営業日以内にご連絡いたします。</p>
			<div class="c-btn1 c-btn1--blue">
				<a class="c-btn1__txt" href="" title=""><span>お問い合わせフォーム</span></a>
			</div>
		</li>
		<li class="c-list2__item">
			<div class="c-ttl4 c-ttl4--white">
				<p class="c-ttl4__icon"><img src="/assets/img/common/icon9-ttl4.png" alt=""></p>
				<h3 class="c-ttl4__txt">資料のご請求</h3>
			</div>
			<p class="c-list2__txt">NSDビジネスイノベーションがご提供する製品・サービスの各種カタログをお申し込みいただけます。</p>
			<div class="c-btn1 c-btn1--blue">
				<a class="c-btn1__txt" href="" title=""><span>資料請求フォーム</span></a>
			</div>
		</li>
	</ul>
</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list3</div>
<div class="l-wrap1">
	<div class="c-list3">
		<ul>
			<li class="c-list3__item">
				<div class="c-list3__cattime">
					<div class="c-cats">
						<p class="c-cats__txt">製品・サービス</p>
					</div>
					<div class="c-time1"><img src="/assets/img/common/icon-clock.png" alt=""><span>2019.03.01</span></div>
				</div>
				<h3 class="c-list3__ttl"><span>SOMPOリスクマネジメント株式会社様より弊社分析ソフト<br>「Act-FLOW」を活用した医療インシデント分析ワークショップ「クラウド ImSAFER」提供開始</span></h3>
				<p class="c-list3__txt">リード文が入ります。この文章はダミーです予めご了承ください。リード文が入ります。この文章はダミーです予めご了承ください。...</p>
				<a class="c-list3__link" href="" title=""></a>
			</li>
			<li class="c-list3__item">
				<div class="c-list3__cattime">
					<div class="c-cats">
						<p class="c-cats__txt">製品・サービス</p>
					</div>
					<div class="c-time1"><img src="/assets/img/common/icon-clock.png" alt=""><span>2019.03.01</span></div>
				</div>
				<h3 class="c-list3__ttl"><span>法人向けファイル共有システム「eTransporter Collabo」を販売開始</span></h3>
				<p class="c-list3__txt">リード文が入ります。この文章はダミーです予めご了承ください。リード文が入ります。この文章はダミーです予めご了承ください。...</p>
				<a class="c-list3__link" href="" title=""></a>
			</li>
			<li class="c-list3__item">
				<div class="c-list3__cattime">
					<div class="c-cats">
						<p class="c-cats__txt">製品・サービス</p>
					</div>
					<div class="c-time1"><img src="/assets/img/common/icon-clock.png" alt=""><span>2019.03.01</span></div>
				</div>
				<h3 class="c-list3__ttl"><span>法人向けファイル共有システム「eTransporter Collabo」をクラウドで提供</span></h3>
				<p class="c-list3__txt">リード文が入ります。この文章はダミーです予めご了承ください。リード文が入ります。この文章はダミーです予めご了承ください。...</p>
				<a class="c-list3__link" href="" title=""></a>
			</li>
		</ul>
	</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list3 c-list3--width</div>
<div class="l-wrap3">
	<div class="c-list3 c-list3--width">
		<ul>
			<li class="c-list3__item">
				<div class="c-list3__cattime">
					<div class="c-cats">
						<p class="c-cats__txt">ニュースリリース</p>
					</div>
					<div class="c-time1"><span>2019.03.01</span></div>
				</div>
				<h3 class="c-list3__ttl"><span>コーポレートサイト並びにサービスページをリニューアルいたしました</span></h3>
				<p class="c-list3__txt">リード文が入ります。この文章はダミーです予めご了承ください。リード文が入ります。この文章はダミーです予めご了承くだささい。...</p>
				<a class="c-list3__link" href="" title=""></a>
			</li>
			<li class="c-list3__item">
				<div class="c-list3__cattime">
					<div class="c-cats">
						<p class="c-cats__txt">ニュースリリース</p>
					</div>
					<div class="c-time1"><span>2019.03.01</span></div>
				</div>
				<h3 class="c-list3__ttl"><span>役員人事に関するお知らせ</span></h3>
				<p class="c-list3__txt">リード文が入ります。この文章はダミーです予めご了承ください。リード文が入ります。この文章はダミーです予めご了承くだささい。...</p>
				<a class="c-list3__link" href="" title=""></a>
			</li>
			<li class="c-list3__item">
				<div class="c-list3__cattime">
					<div class="c-cats">
						<p class="c-cats__txt">製品・サービス</p>
					</div>
					<div class="c-time1"><span>2019.03.01</span></div>
				</div>
				<h3 class="c-list3__ttl"><span>SOMPOリスクマネジメント株式会社様より弊社分析ソフト「Act-FLOW」を活用した医療インシデント分析ワークショップ「クラウド ImSAFER」提供開始</span></h3>
				<p class="c-list3__txt">リード文が入ります。この文章はダミーです予めご了承ください。リード文が入ります。この文章はダミーです予めご了承ください。...</p>
				<a class="c-list3__link" href="" title=""></a>
			</li>
			<li class="c-list3__item">
				<div class="c-list3__cattime">
					<div class="c-cats">
						<p class="c-cats__txt">セミナー・イベント</p>
					</div>
					<div class="c-time1"><span>2019.03.01</span></div>
				</div>
				<h3 class="c-list3__ttl"><span>ヒューマンエラー防止手法セミナー開催のお知らせ</span></h3>
				<p class="c-list3__txt">【通常コース】2017年11月17日（金）／2018年 3月23日（金）【アドバンスコース】2018年 3月24日（土）</p>
				<a class="c-list3__link" href="" title=""></a>
			</li>
			<li class="c-list3__item">
				<div class="c-list3__cattime">
					<div class="c-cats">
						<p class="c-cats__txt">製品・サービス</p>
					</div>
					<div class="c-time1"><span>2019.03.01</span></div>
				</div>
				<h3 class="c-list3__ttl"><span>法人向けファイル共有システム「eTransporter Collabo」を販売開始</span></h3>
				<p class="c-list3__txt">リード文が入ります。この文章はダミーです予めご了承ください。リード文が入ります。この文章はダミーです予めご了承くだささい。...</p>
				<a class="c-list3__link" href="" title=""></a>
			</li>
			<li class="c-list3__item">
				<div class="c-list3__cattime">
					<div class="c-cats">
						<p class="c-cats__txt">製品・サービス</p>
					</div>
					<div class="c-time1"><span>2019.03.01</span></div>
				</div>
				<h3 class="c-list3__ttl"><span>法人向けファイル共有システム「eTransporter Collabo」をクラウドで提供</span></h3>
				<p class="c-list3__txt">リード文が入ります。この文章はダミーです予めご了承ください。リード文が入ります。この文章はダミーです予めご了承くだささい。...</p>
				<a class="c-list3__link" href="" title=""></a>
			</li>
		</ul>
	</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list4</div>
<div class="l-wrap1">
	<div class="c-list4">
		<ul>
			<li class="c-list4__item">
				<div class="c-ttl5">
					<p class="c-ttl5__icon"><img src="/assets/img/common/txt1-ttl4.png" alt=""></p>
					<div class="c-ttl5__detail">
						<h3 class="c-ttl5__txt1">ファイル転送システムに運用コストをかけたくない！25文字前後</h3>
						<ul class="c-ttl5__info">
							<li>
								<span>業種</span>
								<p>サービス業</p>
							</li>
							<li>
								<span>部署</span>
								<p>システム部</p>
							</li>
							<li>
								<span>規模</span>
								<p>500名</p>
							</li>
						</ul>
					</div>
				</div>
				<div class="c-solvetxt">
					<div class="c-solvetxt__row">
						<div class="c-solvetxt__ttl">
							<h5>課題</h5>
						</div>
						<div class="c-solvetxt__info">
							<div class="c-solvetxt__txt">
								<p>課題、お悩みの詳細が入りますこの文章はダミーです。 課題、お悩みの詳細が入りますこの文章はダミーです。課題、お悩みの詳細が入りますこの文章はダミーです。課題、お悩みの詳細が入りますこの文章はダミーです。課題、お悩みの詳細が入りますこの文章はダミーです（100文字前後）</p>
							</div>
						</div>
					</div>
					<div class="c-solvetxt__row">
						<div class="c-solvetxt__ttl">
							<h5>解決策</h5>
						</div>
						<div class="c-solvetxt__info">
							<div class="c-solvetxt__txt">
								<p>解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。（200文字前後）</p>
							</div>
							<div class="c-solvetxt__img">
								<img src="/assets/img/common/img1-imgtxt1.jpg" alt="">
							</div>
						</div>
					</div>
				</div>
				<div class="c-btn2">
					<a class="c-btn2__txt" href="" title=""><span>詳しく見る</span></a>
				</div>
			</li>
			<li class="c-list4__item">
				<div class="c-ttl5">
					<p class="c-ttl5__icon"><img src="/assets/img/common/txt2-ttl4.png" alt=""></p>
					<div class="c-ttl5__detail">
						<h3 class="c-ttl5__txt1">ファイル転送システムに運用コストをかけたくない！25文字前後</h3>
						<ul class="c-ttl5__info">
							<li>
								<span>業種</span>
								<p>サービス業</p>
							</li>
							<li>
								<span>部署</span>
								<p>システム部</p>
							</li>
							<li>
								<span>規模</span>
								<p>500名</p>
							</li>
						</ul>
					</div>
				</div>
				<div class="c-solvetxt">
					<div class="c-solvetxt__row">
						<div class="c-solvetxt__ttl">
							<h5>課題</h5>
						</div>
						<div class="c-solvetxt__info">
							<div class="c-solvetxt__txt">
								<p>課題、お悩みの詳細が入りますこの文章はダミーです。 課題、お悩みの詳細が入りますこの文章はダミーです。課題、お悩みの詳細が入りますこの文章はダミーです。課題、お悩みの詳細が入りますこの文章はダミーです。課題、お悩みの詳細が入りますこの文章はダミーです（100文字前後）</p>
							</div>
						</div>
					</div>
					<div class="c-solvetxt__row">
						<div class="c-solvetxt__ttl">
							<h5>解決策</h5>
						</div>
						<div class="c-solvetxt__info">
							<div class="c-solvetxt__txt">
								<p>解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。（200文字前後）</p>
							</div>
							<div class="c-solvetxt__img">
								<img src="/assets/img/common/img1-imgtxt1.jpg" alt="">
							</div>
						</div>
					</div>
				</div>
				<div class="c-btn2">
					<a class="c-btn2__txt" href="" title=""><span>詳しく見る</span></a>
				</div>
			</li>
			<li class="c-list4__item">
				<div class="c-ttl5">
					<p class="c-ttl5__icon"><img src="/assets/img/common/txt3-ttl4.png" alt=""></p>
					<div class="c-ttl5__detail">
						<h3 class="c-ttl5__txt1">ファイル転送システムに運用コストをかけたくない！25文字前後</h3>
						<ul class="c-ttl5__info">
							<li>
								<span>業種</span>
								<p>サービス業</p>
							</li>
							<li>
								<span>部署</span>
								<p>システム部</p>
							</li>
							<li>
								<span>規模</span>
								<p>500名</p>
							</li>
						</ul>
					</div>
				</div>
				<div class="c-solvetxt">
					<div class="c-solvetxt__row">
						<div class="c-solvetxt__ttl">
							<h5>課題</h5>
						</div>
						<div class="c-solvetxt__info">
							<div class="c-solvetxt__txt">
								<p>課題、お悩みの詳細が入りますこの文章はダミーです。 課題、お悩みの詳細が入りますこの文章はダミーです。課題、お悩みの詳細が入りますこの文章はダミーです。課題、お悩みの詳細が入りますこの文章はダミーです。課題、お悩みの詳細が入りますこの文章はダミーです（100文字前後）</p>
							</div>
						</div>
					</div>
					<div class="c-solvetxt__row">
						<div class="c-solvetxt__ttl">
							<h5>解決策</h5>
						</div>
						<div class="c-solvetxt__info">
							<div class="c-solvetxt__txt">
								<p>解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。（200文字前後）</p>
							</div>
							<div class="c-solvetxt__img">
								<img src="/assets/img/common/img1-imgtxt1.jpg" alt="">
							</div>
						</div>
					</div>
				</div>
				<div class="c-btn2">
					<a class="c-btn2__txt" href="" title=""><span>詳しく見る</span></a>
				</div>
			</li>
		</ul>
	</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list5</div>
<div class="l-wrap1">
	<div class="c-list5">
		<ul>
			<li class="c-list5__item">
				<div class="c-list5__img" style="background: #f4f4f4 url(/assets/img/common/img1-list5.jpg) no-repeat center;">
				</div>
				<div class="c-list5__detail">
					<h3 class="c-list5__ttl">『働き手不足の時代 その対策は…ルーチンワークの撲滅！』</h3>
					<p class="c-list5__txt">リード文が入ります。この文章はダミーです予めご了承下さいこの文章はダミーです予めご了承下さいこの文章はダミーです予めご了承下さい。（80文字前後）</p>
					<div class="c-list5__tagtime">
						<div class="c-time1"><img src="/assets/img/common/icon-clock.png" alt=""><span>2019.03.01 8時間前</span></div>
						<div class="c-tags">
							<a href="" title="">#タグ</a>,<a href="" title=""> #タグ</a>
						</div>
					</div>
					<div class="c-btn2">
						<a class="c-btn2__txt" href="" title=""><span>ダウンロードする</span></a>
					</div>
				</div>
				<div class="c-cats  c-cats--blue1">
					<p class="c-cats__txt">製品・サービス</p>
				</div>
			</li>
			<li class="c-list5__item">
				<div class="c-list5__img" style="background: #f4f4f4 url(/assets/img/common/img1-list5.jpg) no-repeat center;">
				</div>
				<div class="c-list5__detail">
					<h3 class="c-list5__ttl">『働き手不足の時代 その対策は…ルーチンワークの撲滅！』</h3>
					<p class="c-list5__txt">リード文が入ります。この文章はダミーです予めご了承下さいこの文章はダミーです予めご了承下さいこの文章はダミーです予めご了承下さい。（80文字前後）</p>
					<div class="c-list5__tagtime">
						<div class="c-time1"><img src="/assets/img/common/icon-clock.png" alt=""><span>2019.03.01 8時間前</span></div>
						<div class="c-tags">
							<a href="" title="">#タグ</a>,<a href="" title=""> #タグ</a>
						</div>
					</div>
					<div class="c-btn2">
						<a class="c-btn2__txt" href="" title=""><span>ダウンロードする</span></a>
					</div>
				</div>
				<div class="c-cats  c-cats--blue1">
					<p class="c-cats__txt">製品・サービス</p>
				</div>
			</li>
		</ul>
	</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list6</div>
<div class="l-wrap3">
<div class="c-list6">
	<ul>
		<li class="c-list6__item security">
			<div class="c-list6__img">
				<img src="/assets/img/common/logo-etransporter.png" alt="">
			</div>
			<h3 class="c-list6__ttl"><span>ユーザ数無制限の法人向けファイル転送システム</span></h3>
			<p class="c-list6__txt1">eTransporter</p>
			<div class="c-cats c-cats--blue1">
				<p class="c-cats__txt">製品・サービス</p>
			</div>
			<a class="c-list6__link" href="" title=""></a>
		</li>
		<li class="c-list6__item security">
			<div class="c-list6__img">
				<img src="/assets/img/common/logo-etracollabo.png" alt="">
			</div>
			<h3 class="c-list6__ttl"><span>コラボ特化型のオンラインストレージ</span></h3>
			<p class="c-list6__txt1">eTransporter Collabo</p>
			<div class="c-cats c-cats--blue1">
				<p class="c-cats__txt">セキュリティ</p>
			</div>
			<a class="c-list6__link" href="" title=""></a>
		</li>
		<li class="c-list6__item security">
			<div class="c-list6__img">
				<img src="/assets/img/common/logo-ciphercraft.png" alt="">
			</div>
			<h3 class="c-list6__ttl"><span>メール誤送信防止・暗号化</span></h3>
			<p class="c-list6__txt1">CipherCraft/Mail</p>
			<div class="c-cats c-cats--blue1">
				<p class="c-cats__txt">セキュリティ</p>
			</div>
			<a class="c-list6__link" href="" title=""></a>
		</li>
		<li class="c-list6__item security">
			<div class="c-list6__img">
				<img src="/assets/img/common/logo-fileserver.png" alt="">
			</div>
			<h3 class="c-list6__ttl"><span>ファイルサーバーアクセスログ監視ツール</span></h3>
			<p class="c-list6__txt1">File Server Audit</p>
			<div class="c-cats c-cats--blue1">
				<p class="c-cats__txt">セキュリティ</p>
			</div>
			<a class="c-list6__link" href="" title=""></a>
		</li>
		<li class="c-list6__item security">
			<div class="c-list6__img">
				<img src="/assets/img/common/logo-sophos.png" alt="">
			</div>
			<h3 class="c-list6__ttl"><span>次世代型エンドポイントセキュリティ</span></h3>
			<p class="c-list6__txt1">SOPHOS</p>
			<div class="c-cats c-cats--blue1">
				<p class="c-cats__txt">セキュリティ</p>
			</div>
			<a class="c-list6__link" href="" title=""></a>
		</li>
		<li class="c-list6__item security">
			<div class="c-list6__img">
				<img src="/assets/img/common/logo-mcafee.png" alt="">
			</div>
			<h3 class="c-list6__ttl"><span>ライセンス販売から構築・運用支援まで幅広く対応</span></h3>
			<p class="c-list6__txt1">McAfee製品導入支援サービス</p>
			<div class="c-cats c-cats--blue1">
				<p class="c-cats__txt">セキュリティ</p>
			</div>
			<a class="c-list6__link" href="" title=""></a>
		</li>
		<li class="c-list6__item security">
			<div class="c-list6__img">
				<img src="/assets/img/common/logo-idoperation.png" alt="">
			</div>
			<h3 class="c-list6__ttl"><span>監査対応・内部不正対策　特権ID管理ソリューション</span></h3>
			<p class="c-list6__txt1">iDoperation</p>
			<div class="c-cats c-cats--blue1">
				<p class="c-cats__txt">セキュリティ</p>
			</div>
			<a class="c-list6__link" href="" title=""></a>
		</li>
		<li class="c-list6__item security">
			<div class="c-list6__img">
				<img src="/assets/img/common/logo-idoperation-sc.png" alt="">
			</div>
			<h3 class="c-list6__ttl"><span>サーバ・PC操作ログ取得・管理</span></h3>
			<p class="c-list6__txt1">iDoperation SC</p>
			<div class="c-cats c-cats--blue1">
				<p class="c-cats__txt">セキュリティ</p>
			</div>
			<a class="c-list6__link" href="" title=""></a>
		</li>
		<li class="c-list6__item security">
			<div class="c-list6__img">
				<img src="/assets/img/common/logo-evidian.png" alt="">
			</div>
			<h3 class="c-list6__ttl"><span>アクセス制御・二要素認証対応 次世代型シングルサインオン</span></h3>
			<p class="c-list6__txt1">Evidian</p>
			<div class="c-cats c-cats--blue1">
				<p class="c-cats__txt">セキュリティ</p>
			</div>
			<a class="c-list6__link" href="" title=""></a>
		</li>
		<li class="c-list6__item security">
			<div class="c-list6__img">
				<img src="/assets/img/common/logo-swivel.png" alt="">
			</div>
			<h3 class="c-list6__ttl"><span>２要素認証 トークンレス・ワンタイムパスワード</span></h3>
			<p class="c-list6__txt1">swivel secure</p>
			<div class="c-cats c-cats--blue1">
				<p class="c-cats__txt">セキュリティ</p>
			</div>
			<a class="c-list6__link" href="" title=""></a>
		</li>
		<li class="c-list6__item security">
			<div class="c-list6__img">
				<img src="/assets/img/common/logo-proofpoint.png" alt="">
			</div>
			<h3 class="c-list6__ttl"><span>標的型攻撃対策 メールセキュリティソリューション</span></h3>
			<p class="c-list6__txt1">Proofpoint</p>
			<div class="c-cats c-cats--blue1">
				<p class="c-cats__txt">セキュリティ</p>
			</div>
			<a class="c-list6__link" href="" title=""></a>
		</li>
		<li class="c-list6__item security">
			<div class="c-list6__img">
				<img src="/assets/img/common/logo-spc.png" alt="">
			</div>
			<h3 class="c-list6__ttl"><span>月額100円（税別）から利用可 クラウドメール誤送信対策</span></h3>
			<p class="c-list6__txt1">SPC Mailエスティー</p>
			<div class="c-cats c-cats--blue1">
				<p class="c-cats__txt">セキュリティ</p>
			</div>
			<a class="c-list6__link" href="" title=""></a>
		</li>
		<li class="c-list6__item security">
			<div class="c-list6__img">
				<img src="/assets/img/common/logo-venafi.png" alt="">
			</div>
			<h3 class="c-list6__ttl"><span>電子証明書ライフサイクル管理</span></h3>
			<p class="c-list6__txt1">Venafi</p>
			<div class="c-cats c-cats--blue1">
				<p class="c-cats__txt">セキュリティ</p>
			</div>
			<a class="c-list6__link" href="" title=""></a>
		</li>
		<li class="c-list6__item business">
			<div class="c-list6__img">
				<img src="/assets/img/common/logo-epower.png" alt="">
			</div>
			<h3 class="c-list6__ttl"><span>Excelを使った管理業務の効率化・管理コスト削減</span></h3>
			<p class="c-list6__txt1">ePower/exDirector</p>
			<div class="c-cats c-cats--green1">
				<p class="c-cats__txt">業務効率化・コスト削減</p>
			</div>
			<a class="c-list6__link" href="" title=""></a>
		</li>
		<li class="c-list6__item business">
			<div class="c-list6__img">
				<img src="/assets/img/common/logo-share.png" alt="">
			</div>
			<h3 class="c-list6__ttl"><span>Microsoft SharePointによる企業ポータル導入・移行支援</span></h3>
			<p class="c-list6__txt1">SharePoint導入・移行支援サービス</p>
			<div class="c-cats c-cats--green1">
				<p class="c-cats__txt">業務効率化・コスト削減</p>
			</div>
			<a class="c-list6__link" href="" title=""></a>
		</li>
		<li class="c-list6__item business">
			<div class="c-list6__img">
				<img src="/assets/img/common/logo-fineprint.png" alt="">
			</div>
			<h3 class="c-list6__ttl"><span>印刷のムダを無くせる多機能プリンタドライバ</span></h3>
			<p class="c-list6__txt1">FinePrint</p>
			<div class="c-cats c-cats--green1">
				<p class="c-cats__txt">業務効率化・コスト削減</p>
			</div>
			<a class="c-list6__link" href="" title=""></a>
		</li>
		<li class="c-list6__item smart">
			<div class="c-list6__img">
				<img src="/assets/img/common/logo-file.png" alt="">
			</div>
			<h3 class="c-list6__ttl"><span>iPadから簡単・セキュアに社内ファイルサーバ検索・閲覧</span></h3>
			<p class="c-list6__txt1">FileServerPad</p>
			<div class="c-cats c-cats--green2">
				<p class="c-cats__txt">スマートデバイス活用</p>
			</div>
			<a class="c-list6__link" href="" title=""></a>
		</li>
		<li class="c-list6__item">
			
		</li>
		<li class="c-list6__item">
			
		</li>
		<li class="c-list6__item">
			
		</li>
	</ul>
</div>
</div>