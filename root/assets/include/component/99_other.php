<?php /*========================================
other
================================================*/ ?>
<div class="c-dev-title1">other</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-mv1</div>
<div class="c-mv1">
	<div class="c-mv1__img" style="background-image: url(/assets/img/common/img1-mv1.jpg);"></div>
	<div class="c-mv1__detail">
		<h2 class="c-mv1__ttl1">大容量ファイルを<br><b>セキュアに送受信</b></h2>
		<span class="c-mv1__btn1">法人向けファイル転送システム</span>
		<div class="c-mv1__logo1">
			<img src="/assets/img/common/logo-mv1.png" alt="">
		</div>
	</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-cvarea1</div>
<div style="background: #4cbbb5">
<div class="l-wrap3">
	<div class="c-cvarea1">
		<div class="c-cvarea1__info">
			<div class="c-ttl2">
				<div class="c-ttl2__left">
					<h2 class="c-ttl2__txt1"><img src="/assets/img/common/ttl1-cv1.png" alt=""></h2>
					<p class="c-ttl2__txt2">お役立ち資料ダウンロード</p>
				</div>
			</div>
			<p class="c-cvarea1__txt">業務のお悩みや課題の解決に役立つ資料を無料で配布しております。セキュリティ対策や業務効率化、コスト削減など、課題解決に是非お役立てください！</p>
			<div class="c-btn1 c-btn1--blue">
				<a class="c-btn1__txt" href="" title=""><span>お役立ち資料ダウンロード</span></a>
			</div>
		</div>
		<div class="c-cvarea1__img">
			<img src="/assets/img/common/img1-cv1.png" alt="">
		</div>
	</div>
</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-cvarea2</div>
<div style="background: #4cbbb5">
<div class="l-wrap1">
	<div class="c-cvarea2">
		<div class="c-cvarea2__img">
			<img src="/assets/img/common/img1-cv1.png" alt="">
		</div>
		<div class="c-ttl4 c-ttl4--white">
			<p class="c-ttl4__icon">今なら</p>
			<h3 class="c-ttl4__txt">企業セキュリティ対策法が詰まった<br class="pc-only">実践ガイドブックをプレゼント！</h3>
		</div>
		<div class="c-btn1">
			<a class="c-btn1__txt" href="" title=""><span>お役立ち資料ダウンロード</span></a>
		</div>
	</div>
</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-cats and (c-cats--green1 or c-cats--blue1 or c-cats--green2)</div>
<div class="c-cats">
	<p class="c-cats__txt">製品・サービス</p>
</div>
<div class="c-cats c-cats--green1">
	<p class="c-cats__txt">製品・サービス</p>
</div>
<div class="c-cats  c-cats--blue1">
	<p class="c-cats__txt">製品・サービス</p>
</div>
<div class="c-cats c-cats--green2">
	<p class="c-cats__txt">製品・サービス</p>
</div>


<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-solvetxt</div>
<div class="c-solvetxt">
	<div class="c-solvetxt__row">
		<div class="c-solvetxt__ttl">
			<h5>課題</h5>
		</div>
		<div class="c-solvetxt__info">
			<div class="c-solvetxt__txt">
				<p>課題、お悩みの詳細が入りますこの文章はダミーです。 課題、お悩みの詳細が入りますこの文章はダミーです。課題、お悩みの詳細が入りますこの文章はダミーです。課題、お悩みの詳細が入りますこの文章はダミーです。課題、お悩みの詳細が入りますこの文章はダミーです（100文字前後）</p>
			</div>
		</div>
	</div>
	<div class="c-solvetxt__row">
		<div class="c-solvetxt__ttl">
			<h5>解決策</h5>
		</div>
		<div class="c-solvetxt__info">
			<div class="c-solvetxt__txt">
				<p>解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。解決策の説明が入ります。（200文字前後）</p>
			</div>
			<div class="c-solvetxt__img">
				<img src="/assets/img/common/img1-imgtxt1.jpg" alt="">
			</div>
		</div>
	</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-banner1</div>
<section class="c-banner1">
	<div class="l-wrap1">
		<div class="c-cvarea3">
			<h3 class="c-cvarea3__ttl">eTransporter導入で、業務のムダを削減！
			</h3>
			<div class="c-cvarea3__box">
				<div class="c-cvarea3__left">
					<div class="c-ttl3">
						<div class="c-ttl3__rank">
							<p class="c-ttl3__txt3"><img src="/assets/img/common/free-trial.png" alt=""></p>
						</div>
						<h3 class="c-ttl3__txt1">業務効率化を<br>無料体験！</h3>
					</div>
					<p class="c-cvarea3__txt">実際の操作を交えて業務のムダ削減を体感して下さい。実際の操作を交えて業務のムダ削減を（40文字前後）</p>
					<div class="c-btn1">
						<a class="c-btn1__txt" href="" title=""><span>今すぐ無料体験を開始する</span></a>
					</div>
				</div>

				<div class="c-cvarea3__right">
					<img src="/assets/img/common/icon-cv3.png" alt="">
				</div>
			</div>
		</div>
	</div>
</section>


<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-service</div>
<div class="l-wrap3">
<div class="c-service">
	<nav class="c-navi1">
		<ul>
			<li class="c-navi1__tab all is-active" data-tab="all"> <img src="/assets/img/common/icon-all.png" alt=""> すべて</li>
			<li class="c-navi1__tab security" data-tab="security"> <img src="/assets/img/common/icon-security.png" alt=""> セキュリティ</li>
			<li class="c-navi1__tab business" data-tab="business"> <img src="/assets/img/common/icon-data.png" alt=""> 業務効率化・<br>コスト削減</li>
			<li class="c-navi1__tab smart" data-tab="smart"> <img src="/assets/img/common/icon-smart.png" alt=""> スマート<br>デバイス活用</li>
		</ul>
	</nav>
	<div class="c-list6">
		<ul>
			<li class="c-list6__item security">
				<div class="c-list6__img">
					<img src="/assets/img/common/logo-etransporter.png" alt="">
				</div>
				<h3 class="c-list6__ttl"><span>ユーザ数無制限の法人向けファイル転送システム</span></h3>
				<p class="c-list6__txt1">eTransporter</p>
				<div class="c-cats c-cats--blue1">
					<p class="c-cats__txt">製品・サービス</p>
				</div>
				<a class="c-list6__link" href="" title=""></a>
			</li>
			<li class="c-list6__item security">
				<div class="c-list6__img">
					<img src="/assets/img/common/logo-etracollabo.png" alt="">
				</div>
				<h3 class="c-list6__ttl"><span>コラボ特化型のオンラインストレージ</span></h3>
				<p class="c-list6__txt1">eTransporter Collabo</p>
				<div class="c-cats c-cats--blue1">
					<p class="c-cats__txt">セキュリティ</p>
				</div>
				<a class="c-list6__link" href="" title=""></a>
			</li>
			<li class="c-list6__item security">
				<div class="c-list6__img">
					<img src="/assets/img/common/logo-ciphercraft.png" alt="">
				</div>
				<h3 class="c-list6__ttl"><span>メール誤送信防止・暗号化</span></h3>
				<p class="c-list6__txt1">CipherCraft/Mail</p>
				<div class="c-cats c-cats--blue1">
					<p class="c-cats__txt">セキュリティ</p>
				</div>
				<a class="c-list6__link" href="" title=""></a>
			</li>
			<li class="c-list6__item security">
				<div class="c-list6__img">
					<img src="/assets/img/common/logo-fileserver.png" alt="">
				</div>
				<h3 class="c-list6__ttl"><span>ファイルサーバーアクセスログ監視ツール</span></h3>
				<p class="c-list6__txt1">File Server Audit</p>
				<div class="c-cats c-cats--blue1">
					<p class="c-cats__txt">セキュリティ</p>
				</div>
				<a class="c-list6__link" href="" title=""></a>
			</li>
			<li class="c-list6__item security">
				<div class="c-list6__img">
					<img src="/assets/img/common/logo-sophos.png" alt="">
				</div>
				<h3 class="c-list6__ttl"><span>次世代型エンドポイントセキュリティ</span></h3>
				<p class="c-list6__txt1">SOPHOS</p>
				<div class="c-cats c-cats--blue1">
					<p class="c-cats__txt">セキュリティ</p>
				</div>
				<a class="c-list6__link" href="" title=""></a>
			</li>
			<li class="c-list6__item security">
				<div class="c-list6__img">
					<img src="/assets/img/common/logo-mcafee.png" alt="">
				</div>
				<h3 class="c-list6__ttl"><span>ライセンス販売から構築・運用支援まで幅広く対応</span></h3>
				<p class="c-list6__txt1">McAfee製品導入支援サービス</p>
				<div class="c-cats c-cats--blue1">
					<p class="c-cats__txt">セキュリティ</p>
				</div>
				<a class="c-list6__link" href="" title=""></a>
			</li>
			<li class="c-list6__item security">
				<div class="c-list6__img">
					<img src="/assets/img/common/logo-idoperation.png" alt="">
				</div>
				<h3 class="c-list6__ttl"><span>監査対応・内部不正対策　特権ID管理ソリューション</span></h3>
				<p class="c-list6__txt1">iDoperation</p>
				<div class="c-cats c-cats--blue1">
					<p class="c-cats__txt">セキュリティ</p>
				</div>
				<a class="c-list6__link" href="" title=""></a>
			</li>
			<li class="c-list6__item security">
				<div class="c-list6__img">
					<img src="/assets/img/common/logo-idoperation-sc.png" alt="">
				</div>
				<h3 class="c-list6__ttl"><span>サーバ・PC操作ログ取得・管理</span></h3>
				<p class="c-list6__txt1">iDoperation SC</p>
				<div class="c-cats c-cats--blue1">
					<p class="c-cats__txt">セキュリティ</p>
				</div>
				<a class="c-list6__link" href="" title=""></a>
			</li>
			<li class="c-list6__item security">
				<div class="c-list6__img">
					<img src="/assets/img/common/logo-evidian.png" alt="">
				</div>
				<h3 class="c-list6__ttl"><span>アクセス制御・二要素認証対応 次世代型シングルサインオン</span></h3>
				<p class="c-list6__txt1">Evidian</p>
				<div class="c-cats c-cats--blue1">
					<p class="c-cats__txt">セキュリティ</p>
				</div>
				<a class="c-list6__link" href="" title=""></a>
			</li>
			<li class="c-list6__item security">
				<div class="c-list6__img">
					<img src="/assets/img/common/logo-swivel.png" alt="">
				</div>
				<h3 class="c-list6__ttl"><span>２要素認証 トークンレス・ワンタイムパスワード</span></h3>
				<p class="c-list6__txt1">swivel secure</p>
				<div class="c-cats c-cats--blue1">
					<p class="c-cats__txt">セキュリティ</p>
				</div>
				<a class="c-list6__link" href="" title=""></a>
			</li>
			<li class="c-list6__item security">
				<div class="c-list6__img">
					<img src="/assets/img/common/logo-proofpoint.png" alt="">
				</div>
				<h3 class="c-list6__ttl"><span>標的型攻撃対策 メールセキュリティソリューション</span></h3>
				<p class="c-list6__txt1">Proofpoint</p>
				<div class="c-cats c-cats--blue1">
					<p class="c-cats__txt">セキュリティ</p>
				</div>
				<a class="c-list6__link" href="" title=""></a>
			</li>
			<li class="c-list6__item security">
				<div class="c-list6__img">
					<img src="/assets/img/common/logo-spc.png" alt="">
				</div>
				<h3 class="c-list6__ttl"><span>月額100円（税別）から利用可 クラウドメール誤送信対策</span></h3>
				<p class="c-list6__txt1">SPC Mailエスティー</p>
				<div class="c-cats c-cats--blue1">
					<p class="c-cats__txt">セキュリティ</p>
				</div>
				<a class="c-list6__link" href="" title=""></a>
			</li>
			<li class="c-list6__item security">
				<div class="c-list6__img">
					<img src="/assets/img/common/logo-venafi.png" alt="">
				</div>
				<h3 class="c-list6__ttl"><span>電子証明書ライフサイクル管理</span></h3>
				<p class="c-list6__txt1">Venafi</p>
				<div class="c-cats c-cats--blue1">
					<p class="c-cats__txt">セキュリティ</p>
				</div>
				<a class="c-list6__link" href="" title=""></a>
			</li>
			<li class="c-list6__item business">
				<div class="c-list6__img">
					<img src="/assets/img/common/logo-epower.png" alt="">
				</div>
				<h3 class="c-list6__ttl"><span>Excelを使った管理業務の効率化・管理コスト削減</span></h3>
				<p class="c-list6__txt1">ePower/exDirector</p>
				<div class="c-cats c-cats--green1">
					<p class="c-cats__txt">業務効率化・コスト削減</p>
				</div>
				<a class="c-list6__link" href="" title=""></a>
			</li>
			<li class="c-list6__item business">
				<div class="c-list6__img">
					<img src="/assets/img/common/logo-share.png" alt="">
				</div>
				<h3 class="c-list6__ttl"><span>Microsoft SharePointによる企業ポータル導入・移行支援</span></h3>
				<p class="c-list6__txt1">SharePoint導入・移行支援サービス</p>
				<div class="c-cats c-cats--green1">
					<p class="c-cats__txt">業務効率化・コスト削減</p>
				</div>
				<a class="c-list6__link" href="" title=""></a>
			</li>
			<li class="c-list6__item business">
				<div class="c-list6__img">
					<img src="/assets/img/common/logo-fineprint.png" alt="">
				</div>
				<h3 class="c-list6__ttl"><span>印刷のムダを無くせる多機能プリンタドライバ</span></h3>
				<p class="c-list6__txt1">FinePrint</p>
				<div class="c-cats c-cats--green1">
					<p class="c-cats__txt">業務効率化・コスト削減</p>
				</div>
				<a class="c-list6__link" href="" title=""></a>
			</li>
			<li class="c-list6__item smart">
				<div class="c-list6__img">
					<img src="/assets/img/common/logo-file.png" alt="">
				</div>
				<h3 class="c-list6__ttl"><span>iPadから簡単・セキュアに社内ファイルサーバ検索・閲覧</span></h3>
				<p class="c-list6__txt1">FileServerPad</p>
				<div class="c-cats c-cats--green2">
					<p class="c-cats__txt">スマートデバイス活用</p>
				</div>
				<a class="c-list6__link" href="" title=""></a>
			</li>
			<li class="c-list6__item">
				
			</li>
			<li class="c-list6__item">
				
			</li>
			<li class="c-list6__item">
				
			</li>
		</ul>
	</div>
</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-mv2</div>
<div class="c-mv2">
	<div class="l-wrap3">
		<h1 class="c-mv2__ttl">
			システム開発からインフラ構築まで<br>IT課題解決のベストパートナー
		</h1>
		<p class="c-mv2__txt">NBI様は、「どういった課題解決ができるか」「誰向けのソリュー<br>ション/製品なのか」「どの知識レベル向けか」といった文章を掲<br>載いたします。この文章はダミーです予めご了承くださいこの文章<br>はダミーです予めご了承ください。（100文字前後）</p>
	</div>
</div>