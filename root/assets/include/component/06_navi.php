<?php /*========================================
navi
================================================*/ ?>
<div class="c-dev-title1">navi</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-navi1</div>
<div class="l-wrap3">
<nav class="c-navi1">
	<ul>
		<li class="c-navi1__tab all">すべて</li>
		<li class="c-navi1__tab security">セキュリティ</li>
		<li class="c-navi1__tab business">業務効率化・<br>コスト削減</li>
		<li class="c-navi1__tab smart">スマート<br>デバイス活用</li>
	</ul>
</nav>
</div>

