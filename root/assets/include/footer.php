<footer class="c-footer">
	<div class="c-footer-bottom">
		<nav class="c-fnavi">
			<ul>
				<li><a href="" title="">会社情報</a></li>
				<li><a href="" title="">サイトのご利用にあたって</a></li>
				<li><a href="" title="">個人情報の取り扱いについて</a></li>
				<li><a href="" title="">個人情報保護方針</a></li>
				<li><a href="" title="" target="_blank">株式会社NSD <img src="/assets/img/common/icon-blank.png" alt=""></a></li>
			</ul>
		</nav>
		<p class="c-copyright">Copyright © 2019 Business Innovation Co., Ltd. All Rights Reserved.</p>
	</div>
</footer>
<script src="/assets/js/slick.min.js"></script>
<script src="/assets/js/functions.min.js"></script>
</body>
</html>